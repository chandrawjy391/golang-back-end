package routes

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"finalproject/controllers"
	"finalproject/middlewares"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})
	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)
	r.PATCH("/change_password/:id", controllers.ChangePassword)
	rolesMiddlewareRoute := r.Group("/roles")
	rolesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/roles", controllers.GetAllRoles)
	r.GET("/roles/:id", controllers.GetRolesById)
	rolesMiddlewareRoute.POST("/", controllers.CreateRoles)
	rolesMiddlewareRoute.PATCH("/:id", controllers.UpdateRoles)
	rolesMiddlewareRoute.DELETE("/:id", controllers.DeleteRoles)

	phoneMiddlewareRoute := r.Group("/phone")
	phoneMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/phone", controllers.GetAllPhone)
	r.GET("/phone/:id", controllers.GetPhoneById)
	phoneMiddlewareRoute.POST("/", controllers.CreatePhone)
	phoneMiddlewareRoute.PATCH("/:id", controllers.UpdatePhone)
	phoneMiddlewareRoute.DELETE("/:id", controllers.DeletePhone)

	reviewMiddlewareRoute := r.Group("/review")
	reviewMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/review", controllers.GetAllReview)
	r.GET("/review/:id", controllers.GetReviewById)
	reviewMiddlewareRoute.POST("/", controllers.CreateReview)
	reviewMiddlewareRoute.PATCH("/:id", controllers.UpdateReview)
	reviewMiddlewareRoute.DELETE("/:id", controllers.DeleteReview)

	specificationMiddlewareRoute := r.Group("/specification")
	specificationMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/specification", controllers.GetAllSpecification)
	r.GET("/specification/:id", controllers.GetSpecificationById)
	specificationMiddlewareRoute.POST("/", controllers.CreateSpecification)
	specificationMiddlewareRoute.PATCH("/:id", controllers.UpdateSpecification)
	specificationMiddlewareRoute.DELETE("/:id", controllers.DeleteSpecification)

	userroleMiddlewareRoute := r.Group("/user_role")
	userroleMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/user_role", controllers.GetAllUserRoles)
	r.GET("/user_role/:id", controllers.GetUserRolebyId)
	userroleMiddlewareRoute.POST("/", controllers.CreateUserRole)
	userroleMiddlewareRoute.PATCH("/:id", controllers.UpdateUserRole)
	userroleMiddlewareRoute.DELETE("/:id", controllers.DeleteUserRoles)

	vendorMiddlewareRoute := r.Group("/vendor")
	vendorMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/vendor", controllers.GetAllVendor)
	r.GET("/vendor/:id", controllers.GetVendorById)
	vendorMiddlewareRoute.POST("/", controllers.CreateVendor)
	vendorMiddlewareRoute.PATCH("/:id", controllers.UpdateVendor)
	vendorMiddlewareRoute.DELETE("/:id", controllers.DeleteVendor)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
