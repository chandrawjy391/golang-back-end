package models

import "time"

type Phone struct {
	ID              uint          `json:"id" gorm:"primary_key"`
	Model           string        `json:"model"`
	Price           uint          `json:"price"`
	CreatedAt       time.Time     `json:"created_at"`
	UpdatedAt       time.Time     `json:"updated_at"`
	VendorID        uint          `json:"vendor_id"`
	SpecificationID uint          `json:"specification_id"`
	Vendor          Vendor        `json:"-"`
	Specification   Specification `json:"-"`
	Review          []Review      `json:"-"`
}
