package models

import "time"

type Review struct {
	ID         uint      `json:"id"`
	UserID     uint      `json:"user_id"`
	PhoneID    uint      `json:"phone_id"`
	ReviewText string    `json:"review_text"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	User       User      `json:"-"`
	Phone      Phone     `json:"-"`
}
