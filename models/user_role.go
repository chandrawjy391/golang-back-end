package models

import "time"

type UserRole struct {
	ID        uint      `json:"id"`
	UserID    uint      `json:"user_id"`
	RolesID   uint      `json:"roles_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	User      User      `json:"-"`
	Roles     Roles     `json:"-"`
}
