package models

import "time"

type Specification struct {
	ID        uint      `json:"id"`
	Body      string    `json:"body"`
	Display   string    `json:"string"`
	Channel   string    `json:"channel"`
	Memory    string    `json:"memory"`
	OS        string    `json:"os"`
	Battery   string    `json:"battery"`
	Camera    string    `json:"camera"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Phone     []Phone   `json:"phone"`
}
