package controllers

import (
	"finalproject/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type VendorInput struct {
	Name string `json:"name"`
}

// GetAllVendor godoc
// @Summary Get all Vendor.
// @Description Get a list of Vendor.
// @Tags Vendor
// @Produce json
// @Success 200 {object} []models.Vendor
// @Router /vendor [get]
func GetAllVendor(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var Vendor []models.Vendor
	db.Find(&Vendor)

	c.JSON(http.StatusOK, gin.H{"data": Vendor})
}

// CreateVendor godoc
// @Summary Create New Vendor.
// @Description Creating a new Vendor.
// @Tags Vendor
// @Param Body body VendorInput true "the body to create a new Vendor"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Vendor
// @Router /vendor [post]
func CreateVendor(c *gin.Context) {
	// Validate input
	var input VendorInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Vendor
	Vendor := models.Vendor{Name: input.Name}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&Vendor)

	c.JSON(http.StatusOK, gin.H{"data": Vendor})
}

// GetVendorById godoc
// @Summary Get Vendor.
// @Description Get an Vendor by id.
// @Tags Vendor
// @Produce json
// @Param id path string true "Vendor id"

// @Success 200 {object} models.Vendor
// @Router /vendor/{id} [get]
func GetVendorById(c *gin.Context) { // Get model if exist
	var Vendor models.Vendor

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&Vendor).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": Vendor})
}

// UpdateVendor godoc
// @Summary Update Vendor.
// @Description Update Vendor by id.
// @Tags Vendor
// @Produce json
// @Param id path string true "Vendor id"
// @Param Body body VendorInput true "the body to update Vendor"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Vendor
// @Router /vendor/{id} [patch]
func UpdateVendor(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var Vendor models.Vendor
	if err := db.Where("id = ?", c.Param("id")).First(&Vendor).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input VendorInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Vendor
	updatedInput.Name = input.Name

	updatedInput.UpdatedAt = time.Now()

	db.Model(&Vendor).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": Vendor})
}

// DeleteVendor godoc
// @Summary Delete one Vendor.
// @Description Delete a Vendor by id.
// @Tags Vendor
// @Produce json
// @Param id path string true "Vendor id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /vendor/{id} [delete]
func DeleteVendor(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var Vendor models.Vendor
	if err := db.Where("id = ?", c.Param("id")).First(&Vendor).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&Vendor)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
