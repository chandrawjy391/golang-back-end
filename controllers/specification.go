package controllers

import (
	"finalproject/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type SpecificationInput struct {
	Body    string `json:"body"`
	Display string `json:"string"`
	Channel string `json:"channel"`
	Memory  string `json:"memory"`
	OS      string `json:"os"`
	Battery string `json:"battery"`
	Camera  string `json:"camera"`
}

// GetAllSpecification godoc
// @Summary Get all Specification.
// @Description Get a list of Specification.
// @Tags Specification
// @Produce json
// @Success 200 {object} []models.Specification
// @Router /specification [get]
func GetAllSpecification(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var Specification []models.Specification
	db.Find(&Specification)

	c.JSON(http.StatusOK, gin.H{"data": Specification})
}

// CreateSpecification godoc
// @Summary Create New Specification.
// @Description Creating a new Specification.
// @Tags Specification
// @Param Body body SpecificationInput true "the body to create a new Specification"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Specification
// @Router /specification [post]
func CreateSpecification(c *gin.Context) {
	// Validate input
	var input SpecificationInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Specification
	Specification := models.Specification{Body: input.Body, Display: input.Display, Channel: input.Channel, Memory: input.Memory, OS: input.OS, Battery: input.Battery, Camera: input.Camera}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&Specification)

	c.JSON(http.StatusOK, gin.H{"data": Specification})
}

// GetSpecificationById godoc
// @Summary Get Specification.
// @Description Get an Specification by id.
// @Tags Specification
// @Produce json
// @Param id path string true "Specification id"

// @Success 200 {object} models.Specification
// @Router /specification/{id} [get]
func GetSpecificationById(c *gin.Context) { // Get model if exist
	var Specification models.Specification

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&Specification).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": Specification})
}

// UpdateSpecification godoc
// @Summary Update Specification.
// @Description Update Specification by id.
// @Tags Specification
// @Produce json
// @Param id path string true "Specification id"
// @Param Body body SpecificationInput true "the body to update age Specification category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Specification
// @Router /specification/{id} [patch]
func UpdateSpecification(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var Specification models.Specification
	if err := db.Where("id = ?", c.Param("id")).First(&Specification).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input SpecificationInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Specification
	updatedInput.Body = input.Body
	updatedInput.Display = input.Display
	updatedInput.Channel = input.Channel
	updatedInput.OS = input.OS
	updatedInput.Battery = input.Battery
	updatedInput.Camera = input.Camera

	updatedInput.UpdatedAt = time.Now()

	db.Model(&Specification).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": Specification})
}

// DeleteSpecification godoc
// @Summary Delete one Specification.
// @Description Delete a Specification by id.
// @Tags Specification
// @Produce json
// @Param id path string true "Specification id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /specification/{id} [delete]
func DeleteSpecification(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var Specification models.Specification
	if err := db.Where("id = ?", c.Param("id")).First(&Specification).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&Specification)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
