package controllers

import (
	"finalproject/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type RolesInput struct {
	Name string `json:"name"`
}

// GetAllRoles godoc
// @Summary Get all Roles.
// @Description Get a list of Roles.
// @Tags Roles
// @Produce json
// @Success 200 {object} []models.Roles
// @Router /roles [get]
func GetAllRoles(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var Roles []models.Roles
	db.Find(&Roles)

	c.JSON(http.StatusOK, gin.H{"data": Roles})
}

// CreateRoles godoc
// @Summary Create New Roles.
// @Description Creating a new Roles.
// @Tags Roles
// @Param Body body RolesInput true "the body to create a new Roles"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Roles
// @Router /roles [post]
func CreateRoles(c *gin.Context) {
	// Validate input
	var input RolesInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Roles
	Roles := models.Roles{Name: input.Name}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&Roles)

	c.JSON(http.StatusOK, gin.H{"data": Roles})
}

// GetRolesById godoc
// @Summary Get Roles.
// @Description Get an Roles by id.
// @Tags Roles
// @Produce json
// @Param id path string true "Roles id"
// @Success 200 {object} models.Roles
// @Router /roles/{id} [get]
func GetRolesById(c *gin.Context) { // Get model if exist
	var Roles models.Roles

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&Roles).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": Roles})
}

// UpdateRoles godoc
// @Summary Update Roles.
// @Description Update Roles by id.
// @Tags Roles
// @Produce json
// @Param id path string true "Roles id"
// @Param Body body RolesInput true "the body to update age Roles category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Roles
// @Router /roles/{id} [patch]
func UpdateRoles(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var Roles models.Roles
	if err := db.Where("id = ?", c.Param("id")).First(&Roles).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input RolesInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Roles
	updatedInput.Name = input.Name

	updatedInput.UpdatedAt = time.Now()

	db.Model(&Roles).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": Roles})
}

// DeleteRoles godoc
// @Summary Delete one Roles.
// @Description Delete a Roles by id.
// @Tags Roles
// @Produce json
// @Param id path string true "Roles id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /roles/{id} [delete]
func DeleteRoles(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var Roles models.Roles
	if err := db.Where("id = ?", c.Param("id")).First(&Roles).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&Roles)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
