package controllers

import (
	"finalproject/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ReviewInput struct {
	UserID     uint   `json:"user_id"`
	PhoneID    uint   `json:"phone_id"`
	ReviewText string `json:"review_text"`
}

// GetAllReview godoc
// @Summary Get all Review.
// @Description Get a list of Review.
// @Tags Review
// @Produce json
// @Success 200 {object} []models.Review
// @Router /review [get]
func GetAllReview(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var Review []models.Review
	db.Find(&Review)

	c.JSON(http.StatusOK, gin.H{"data": Review})
}

// CreateReview godoc
// @Summary Create New Review.
// @Description Creating a new Review.
// @Tags Review
// @Param Body body ReviewInput true "the body to create a new Review"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Review
// @Router /review [post]
func CreateReview(c *gin.Context) {
	// Validate input
	var input ReviewInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Review
	Review := models.Review{UserID: input.UserID, PhoneID: input.PhoneID, ReviewText: input.ReviewText}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&Review)

	c.JSON(http.StatusOK, gin.H{"data": Review})
}

// GetReviewById godoc
// @Summary Get Review.
// @Description Get an Review by id.
// @Tags Review
// @Produce json
// @Param id path string true "Review id"
// @Success 200 {object} models.Review
// @Router /review/{id} [get]
func GetReviewById(c *gin.Context) { // Get model if exist
	var Review models.Review

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&Review).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": Review})
}

// UpdateReview godoc
// @Summary Update Review.
// @Description Update Review by id.
// @Tags Review
// @Produce json
// @Param id path string true "Review id"
// @Param Body body ReviewInput true "the body to update age Review category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Review
// @Router /review/{id} [patch]
func UpdateReview(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var Review models.Review
	if err := db.Where("id = ?", c.Param("id")).First(&Review).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input ReviewInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Review
	updatedInput.UserID = input.UserID
	updatedInput.PhoneID = input.PhoneID
	updatedInput.ReviewText = input.ReviewText

	updatedInput.UpdatedAt = time.Now()

	db.Model(&Review).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": Review})
}

// DeleteReview godoc
// @Summary Delete one Review.
// @Description Delete a Review by id.
// @Tags Review
// @Produce json
// @Param id path string true "Review id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /review/{id} [delete]
func DeleteReview(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var Review models.Review
	if err := db.Where("id = ?", c.Param("id")).First(&Review).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&Review)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
