package controllers

import (
	"finalproject/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type PhoneInput struct {
	Model           string `json:"model"`
	Price           uint   `json:"price"`
	VendorID        uint   `json:"vendor_id"`
	SpecificationID uint   `json:"specification_id"`
}

// GetAllPhone godoc
// @Summary Get all Phone.
// @Description Get a list of Phone.
// @Tags Phone
// @Produce json
// @Success 200 {object} []models.Phone
// @Router /phone [get]
func GetAllPhone(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var phone []models.Phone
	db.Find(&phone)

	c.JSON(http.StatusOK, gin.H{"data": phone})
}

// CreatePhone godoc
// @Summary Create New Phone.
// @Description Creating a new Phone.
// @Tags Phone
// @Param Body body PhoneInput true "the body to create a new Phone"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Phone
// @Router /phone [post]
func CreatePhone(c *gin.Context) {
	// Validate input
	var input PhoneInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create phone
	phone := models.Phone{Model: input.Model, Price: input.Price, VendorID: input.VendorID, SpecificationID: input.SpecificationID}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&phone)

	c.JSON(http.StatusOK, gin.H{"data": phone})
}

// GetPhoneById godoc
// @Summary Get GetPhone.
// @Description Get an Phone by id.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} models.Phone
// @Router /phone/{id} [get]
func GetPhoneById(c *gin.Context) { // Get model if exist
	var phone models.Phone

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&phone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": phone})
}

// UpdatePhone godoc
// @Summary Update Phone.
// @Description Update Phone by id.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Param Body body PhoneInput true "the body to update age phone category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Phone
// @Router /phone/{id} [patch]
func UpdatePhone(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var phone models.Phone
	if err := db.Where("id = ?", c.Param("id")).First(&phone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input PhoneInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Phone
	updatedInput.Model = input.Model
	updatedInput.Price = input.Price
	updatedInput.VendorID = input.VendorID
	updatedInput.SpecificationID = input.SpecificationID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&phone).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": phone})
}

// DeletePhone godoc
// @Summary Delete one Phone.
// @Description Delete a Phone by id.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /phone/{id} [delete]
func DeletePhone(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var phone models.Phone
	if err := db.Where("id = ?", c.Param("id")).First(&phone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&phone)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
