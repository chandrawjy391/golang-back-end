package controllers

import (
	"finalproject/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type UserRoleInput struct {
	UserID  uint `json:"user_id"`
	RolesID uint `json:"roles_id"`
}

// GetAllUserRoles godoc
// @Summary Get all User Role.
// @Description Get a list of User Role.
// @Tags User Role
// @Produce json
// @Success 200 {object} []models.UserRole
// @Router /user_role [get]
func GetAllUserRoles(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var UserRole []models.UserRole
	db.Find(&UserRole)

	c.JSON(http.StatusOK, gin.H{"data": UserRole})
}

// CreateUserRole godoc
// @Summary Create New User Role.
// @Description Creating a new User Role.
// @Tags User Role
// @Param Body body UserRoleInput true "the body to create a new User Role"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.UserRole
// @Router /user_role [post]
func CreateUserRole(c *gin.Context) {
	// Validate input
	var input UserRoleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create UserRole
	UserRole := models.UserRole{UserID: input.UserID, RolesID: input.RolesID}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&UserRole)

	c.JSON(http.StatusOK, gin.H{"data": UserRole})
}

// GetUserRoleById godoc
// @Summary Get User Role.
// @Description Get an User Role by id.
// @Tags User Role
// @Produce json
// @Param id path string true "User Role id"
// @Success 200 {object} models.UserRole
// @Router /user_role/{id} [get]
func GetUserRolebyId(c *gin.Context) { // Get model if exist
	var UserRole models.UserRole

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&UserRole).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": UserRole})
}

// UpdateUserRole godoc
// @Summary Update User Role.
// @Description Update User Role by id.
// @Tags User Role
// @Produce json
// @Param id path string true "User Role id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body UserRoleInput true "the body to update age User Role category"
// @Success 200 {object} models.UserRole
// @Router /user_role/{id} [patch]
func UpdateUserRole(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var UserRole models.UserRole
	if err := db.Where("id = ?", c.Param("id")).First(&UserRole).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input UserRoleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.UserRole
	updatedInput.UserID = input.UserID
	updatedInput.RolesID = input.RolesID

	updatedInput.UpdatedAt = time.Now()

	db.Model(&UserRole).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": UserRole})
}

// DeleteUserRoles godoc
// @Summary Delete one User Roles.
// @Description Delete a User Roles by id.
// @Tags User Roles
// @Produce json
// @Param id path string true "User Roles id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /user_role/{id} [delete]
func DeleteUserRoles(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var UserRole models.UserRole
	if err := db.Where("id = ?", c.Param("id")).First(&UserRole).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&UserRole)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
